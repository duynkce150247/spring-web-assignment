package com.duynk.springweb.service;

import com.duynk.springweb.dao.CartItemRepository;
import com.duynk.springweb.entity.CartItem;
import com.duynk.springweb.entity.CartSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class CartItemServiceImpl implements CartItemService {

    private final CartItemRepository cartItemRepository;

    @Autowired
    public CartItemServiceImpl (CartItemRepository cartItemRepository){
        this.cartItemRepository = cartItemRepository;
    }

    @Override
    public void delete(CartItem cartitem) {
        cartItemRepository.delete(cartitem);
    }

    @Override
    public void deleteByCartSession(int id) {
        cartItemRepository.deleteByCartSessionId(id);
    }

    @Override
    public CartItem findCartById(int id) {
        Optional<CartItem> result = cartItemRepository.findById(id);

        CartItem cartItem = null;
        if (result.isPresent()) {
            cartItem = result.get();
        }

        return cartItem;
    }
}
