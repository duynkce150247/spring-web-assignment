package com.duynk.springweb.service;


import com.duynk.springweb.entity.Product;
import org.springframework.data.domain.Page;

public interface ProductService {

    Page<Product> findAll(int pageNum);

    Product findById(int id);

    void save(Product product);

    Product findLastInsert();

}
