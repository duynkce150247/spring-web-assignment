package com.duynk.springweb.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.duynk.springweb.entity.User;
import com.duynk.springweb.user.ValidationUser;

public interface UserService extends UserDetailsService {

	User findByUserName(String userName);

	void save(ValidationUser validationUser);
}
