package com.duynk.springweb.service;

import com.duynk.springweb.dao.ProductInsertRepository;
import com.duynk.springweb.dao.ProductRepository;
import com.duynk.springweb.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService{

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Autowired
    private ProductInsertRepository productInsertRepository;

    @Autowired
    private Environment env;

    @Override
    public Page<Product> findAll(int pageNum) {

        // page size = 20, read from application.properties file
        int pageSize = Integer.parseInt(Objects.requireNonNull(env.getProperty("page.size")));

        Pageable pageable = PageRequest.of(pageNum - 1, pageSize);

        return productRepository.findAll(pageable);
    }

    @Override
    public Product findById(int id) {
        Optional<Product> result = productRepository.findById(id);

        Product product = null;
        if (result.isPresent()) {
            product = result.get();
        }
        return product;
    }

    @Override
    @Transactional
    public void save(Product product) {
        productInsertRepository.save(product);
    }

    @Override
    public Product findLastInsert() {
        return productRepository.findTopByOrderByIdDesc();
    }
}
