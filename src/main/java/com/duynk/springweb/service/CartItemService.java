package com.duynk.springweb.service;

import com.duynk.springweb.entity.CartItem;

public interface CartItemService {

    void delete(CartItem cartItem);

    void deleteByCartSession(int id);

    CartItem findCartById(int id);
}
