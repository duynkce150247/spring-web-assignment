package com.duynk.springweb.service;

import com.duynk.springweb.dao.CartSessionRepository;
import com.duynk.springweb.entity.CartSession;
import com.duynk.springweb.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartSessionServiceImpl implements CartSessionService{

    private final CartSessionRepository cartSessionRepository;

    @Autowired
    public CartSessionServiceImpl(CartSessionRepository cartSessionRepository) {
        this.cartSessionRepository = cartSessionRepository;
    }

    @Override
    public CartSession findById(int id) {
        Optional<CartSession> result = cartSessionRepository.findById(id);

        CartSession cartSession = null;
        if (result.isPresent()) {
            cartSession = result.get();
        } else {
            throw new RuntimeException("Did not find cart session with id: " + id);
        }
        return cartSession;
    }

    @Override
    public CartSession findCartSessionByUser(User user) {
        return cartSessionRepository.findCartSessionByUser(user);
    }

    @Override
    public void save(CartSession cartSession) {
        cartSessionRepository.save(cartSession);
    }
}
