package com.duynk.springweb.service;

import com.duynk.springweb.entity.CartSession;
import com.duynk.springweb.entity.User;

public interface CartSessionService {

    CartSession findById(int id);

    CartSession findCartSessionByUser(User user);

    void save(CartSession cartSession);

}
