package com.duynk.springweb.dao;

import com.duynk.springweb.entity.Role;

public interface RoleDao {

	public Role findRoleByName(String theRoleName);
	
}
