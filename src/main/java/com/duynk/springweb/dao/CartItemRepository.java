package com.duynk.springweb.dao;

import com.duynk.springweb.entity.CartItem;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface CartItemRepository extends JpaRepository<CartItem, Integer> {

    @Transactional
    @Modifying
    @Query("DELETE CartItem ci WHERE ci.cartSession.id = ?1")
    void deleteByCartSessionId(int id);
}