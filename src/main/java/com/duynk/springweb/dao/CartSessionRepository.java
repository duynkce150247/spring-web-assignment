package com.duynk.springweb.dao;

import com.duynk.springweb.entity.CartSession;
import com.duynk.springweb.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.EntityManager;

public interface CartSessionRepository extends JpaRepository<CartSession, Integer> {

    CartSession findCartSessionByUser(User user);

}