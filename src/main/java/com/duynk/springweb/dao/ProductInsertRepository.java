package com.duynk.springweb.dao;

import com.duynk.springweb.entity.Product;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class ProductInsertRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public void save(Product product) {
        entityManager.createNativeQuery("INSERT INTO product ( product_name, unit_price, units_in_stock, description , manufacturer , category, created_at, `condition`, image) VALUES (?,?,?,?,?,?,?,?,?)")
                .setParameter(1, product.getProductName())
                .setParameter(2, product.getUnitPrice())
                .setParameter(3, product.getUnitsInStock())
                .setParameter(4, product.getDescription())
                .setParameter(5, product.getManufacturer())
                .setParameter(6, product.getCategory())
                .setParameter(7, product.getCreateAt())
                .setParameter(8, product.getCondition())
                .setParameter(9, product.getImage())
                .executeUpdate();
    }
}
