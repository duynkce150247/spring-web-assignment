package com.duynk.springweb.dao;

import com.duynk.springweb.entity.User;

public interface UserDao {

    public User findByUserName(String userName);
    
    public void save(User user);
    
}
