package com.duynk.springweb.dao;

import com.duynk.springweb.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer>{
    Product findTopByOrderByIdDesc();

    Page<Product> findAll(Pageable pageable);
}