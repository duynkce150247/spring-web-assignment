package com.duynk.springweb.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;


import com.duynk.springweb.entity.User;


@Repository
public class UserDaoImpl implements UserDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public User findByUserName(String username) {
		// get the current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);

		// retrieve/read from database using username
		Query<User> theQuery = currentSession.createQuery("from User where userName=:uName", User.class);
		theQuery.setParameter("uName", username);
		User theUser = null;
		try {
			theUser = theQuery.getSingleResult();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return theUser;
	}

	@Override
	public void save(User user) {
		// get current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);
		
		// create the user
		currentSession.saveOrUpdate(user);
	}

}
