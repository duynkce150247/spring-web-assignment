package com.duynk.springweb.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="cart_session")
public class CartSession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "total")
    private int total;

    @Column(name = "created_at")
    private Date createAt;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "cartSession", cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH })
    private List<CartItem> cartItems;

    public CartSession() {
    }

    public CartSession(int total, Date createAt, User user) {
        this.total = total;
        this.createAt = createAt;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    public void addCartItem(CartItem cartItem, boolean isInCart) {
        if (cartItems == null) {
            cartItems = new ArrayList<>();
        } else {
            int cartId = cartItem.getProduct().getId();
            for (CartItem item : cartItems) {
                if (item.getProduct().getId() == cartId) {
                    item.setQuantityPlus(1);
                    break;
                }
            }
        }
        if (!isInCart){
            cartItems.add(cartItem);
        }
        cartItem.setCartSession(this);
        setTotal();
    }

    public void deleteCartItem(CartItem cartItem){
        cartItems.remove(cartItem);
        setTotal();
    }

    public void deleteAllCartItem(){
        cartItems = new ArrayList<>();
        setTotal();
    }

    public void setTotal() {
        this.total = 0;
        for (CartItem item: cartItems) {
            this.total += item.getQuantity() * item.getProduct().getUnitPrice();
        }
    }

    public boolean isCartItemsContain(CartItem ci){
        return cartItems.stream()
                .filter(item -> ci.getProduct().getId() == item.getProduct().getId())
                .findAny()
                .orElse(null) != null;
    }

    @Override
    public String toString() {
        return "CartSession{" +
                "id=" + id +
                ", total=" + total +
                ", createAt=" + createAt +
                ", user=" + user +
                ", cartItems=" + cartItems +
                '}';
    }
}
