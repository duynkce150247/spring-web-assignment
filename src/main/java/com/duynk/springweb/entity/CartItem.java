package com.duynk.springweb.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="cart_item")
public class CartItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "created_at")
    private Date createAt;

    @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH })
    @JoinColumn(name = "session_id")
    private CartSession cartSession;

    @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH })
    @JoinColumn(name = "product_id")
    private Product product;

    public CartItem() {
    }

    public CartItem(int quantity, Date createAt, CartSession cartSession, Product product) {
        this.quantity = quantity;
        this.createAt = createAt;
        this.cartSession = cartSession;
        this.product = product;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setQuantityPlus(int quantity) {
        this.quantity += quantity;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public CartSession getCartSession() {
        return cartSession;
    }

    public void setCartSession(CartSession cartSession) {
        this.cartSession = cartSession;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "CartItem{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", createAt=" + createAt +
                ", cartSession=" + cartSession +
                ", product=" + product +
                '}';
    }
}
