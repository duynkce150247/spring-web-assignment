package com.duynk.springweb.entity;

import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Entity
@Table(name="product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotEmpty(message = "Product name not empty")
    @Column(name = "product_name")
    private String productName;

    @Min(value = 1, message = "Value must greater than 0")
    @Max(value = 999999999,  message = "Value must smaller than 999999999")
    @Column(name = "unit_price")
    private int unitPrice;

    @Min(value = 1, message = "Value must greater than 0")
    @Max(value = 999999999,  message = "Value must smaller than 999999999")
    @Column(name = "units_in_stock")
    private int unitsInStock;

    @NotEmpty(message = "Description not empty")
    @Column(name = "description")
    private String description;

    @NotEmpty(message = "Manufacturer not empty")
    @Column(name = "manufacturer")
    private String manufacturer;

    @NotEmpty(message = "Category not empty")
    @Column(name = "category")
    private String category;

    @Column(name = "created_at")
    private Date createAt;

    @Column(name = "condition")
    private String condition;

    @Column(name = "image")
    private String image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getUnitsInStock() {
        return unitsInStock;
    }

    public void setUnitsInStock(int unitsInStock) {
        this.unitsInStock = unitsInStock;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", unitPrice=" + unitPrice +
                ", unitInStock=" + unitsInStock +
                ", description='" + description + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", category='" + category + '\'' +
                ", createAt=" + createAt +
                ", condition='" + condition + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
