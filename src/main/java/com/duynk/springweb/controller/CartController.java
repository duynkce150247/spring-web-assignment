package com.duynk.springweb.controller;

import com.duynk.springweb.entity.CartItem;
import com.duynk.springweb.entity.CartSession;
import com.duynk.springweb.entity.Product;
import com.duynk.springweb.entity.User;
import com.duynk.springweb.service.CartItemService;
import com.duynk.springweb.service.CartSessionService;
import com.duynk.springweb.service.ProductService;
import com.duynk.springweb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.sql.Timestamp;

@Controller
@RequestMapping("cart")
public class CartController {

    private final ProductService productService;

    @Autowired
    private CartSessionService cartSessionService;

    @Autowired
    private UserService userService;

    @Autowired
    private CartItemService cartItemService;

    private Principal principal = null;

    @Autowired
    public CartController(ProductService productService) {
        this.productService = productService;
    }

    public Principal getPrincipal(HttpServletRequest request){
        return request.getUserPrincipal();
    }

    @GetMapping(value = "/addProductToCart")
    public String addProductToCart(@RequestParam("productId") int id, HttpServletRequest request) {
        principal = getPrincipal(request);
        if (principal == null){
            return "redirect:/login";
        }

        Product product = productService.findById(id);

        User user = userService.findByUserName(principal.getName());

        CartSession cartSession = cartSessionService.findCartSessionByUser(user);

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        CartItem cartItem = new CartItem(1, timestamp, cartSession, product);

        boolean isInCart = cartSession.isCartItemsContain(cartItem);

        cartSession.addCartItem(cartItem, isInCart);

        cartSession.setTotal();

        cartSessionService.save(cartSession);

        return "redirect:/";
    }

    @GetMapping(value="/view")
    public String viewCart( Model model, HttpServletRequest request) {
        principal = getPrincipal(request);
        if (principal == null){
            return "redirect:/login";
        }

        User user = userService.findByUserName(principal.getName());

        CartSession cartSession = cartSessionService.findCartSessionByUser(user);

        model.addAttribute("cart", cartSession);

        return "cart";
    }

    @GetMapping(value="/remove")
    public String remove(@RequestParam("cartItemId") int id, HttpServletRequest request) {
        principal = getPrincipal(request);
        if (principal == null){
            return "redirect:/login";
        }

        User user = userService.findByUserName(principal.getName());

        CartSession cartSession = cartSessionService.findCartSessionByUser(user);

        CartItem cartItem = cartItemService.findCartById(id);

        if (cartItem == null){
            return "redirect:/error";
        }


        cartItemService.delete(cartItem);

        cartSession.deleteCartItem(cartItem);
        cartSessionService.save(cartSession);

        return "redirect:/cart/view";
    }

    @GetMapping(value="/removeAll")
    public String removeAll(HttpServletRequest request) {
        principal = getPrincipal(request);
        if (principal == null){
            return "redirect:/login";
        }

        User user = userService.findByUserName(principal.getName());

        CartSession cartSession = cartSessionService.findCartSessionByUser(user);

        if (cartSession.getCartItems().isEmpty()){
            return "redirect:/cart/view";
        }
        cartItemService.deleteByCartSession(cartSession.getId());

        cartSession.deleteAllCartItem();
        cartSessionService.save(cartSession);

        return "redirect:/cart/view";
    }
}
