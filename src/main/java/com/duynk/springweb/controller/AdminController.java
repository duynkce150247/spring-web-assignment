package com.duynk.springweb.controller;

import com.duynk.springweb.entity.Product;
import com.duynk.springweb.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;

@Controller
@RequestMapping("admin")
public class AdminController {

    private final ProductService productService;

    @Autowired
    public AdminController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/addProduct")
    public String showFormForAdd(Model model) {

        Product product = new Product();
        product.setCondition("New");
        model.addAttribute("product", product);

        return "product-add";
    }

    @PostMapping("/save")
    public String processProductForm(@Valid @ModelAttribute("product") Product product, BindingResult bindingResult, @RequestParam("fileImage") MultipartFile multipartFile) throws IOException {
        if (bindingResult.hasErrors()) {
            return "product-add";
        }

        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());

        product.setImage(fileName);

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        product.setCreateAt(timestamp);

        productService.save(product);

        product = productService.findLastInsert();
        String uploadDir = "product-images/" + product.getId();

        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }

        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            System.out.println(filePath.toFile().getAbsolutePath());
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new IOException("Could not save uploaded file: " + fileName);
        }

        return "redirect:/";
    }
}
