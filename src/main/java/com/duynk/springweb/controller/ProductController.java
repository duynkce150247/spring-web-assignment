package com.duynk.springweb.controller;

import com.duynk.springweb.entity.Product;
import com.duynk.springweb.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping(value = {"product"})
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/list")
    public String viewHomePage(Model model) {
        return viewPageProduct(1, model);
    }

    @GetMapping("/list/page")
    public String viewPageProduct(@RequestParam(name = "pageNum") int pageNum, Model model) {

        Page<Product> page = productService.findAll(pageNum);

        List<Product> products = page.stream().toList();

        model.addAttribute("currentPage", pageNum);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("products", products);

        return "products-list";
    }


    @GetMapping("/detail")
    public String findById(@RequestParam("productId") int id, Model model) {

        Product product = productService.findById(id);

        model.addAttribute("product", product);

        return "product-detail";
    }
}
